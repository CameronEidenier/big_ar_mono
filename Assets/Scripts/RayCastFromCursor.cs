﻿using UnityEngine;
using System.Collections;

public class RayCastFromCursor : MonoBehaviour
{
    bool enter = true;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Ray spriteCursorRay = new Ray(transform.position, transform.forward);
        RaycastHit SpriteCursorRaycastHit;
        Physics.Raycast(spriteCursorRay, out SpriteCursorRaycastHit);
        if (SpriteCursorRaycastHit.collider != null){
            if (SpriteCursorRaycastHit.collider.tag == "Hit Object")
            {
                
            }
            if (Cardboard.SDK.Triggered && SpriteCursorRaycastHit.collider.tag == "Hit Object")
            {
                print("Hit the sphere");
                SpriteCursorRaycastHit.collider.GetComponent<CreateObject>().SpawnObject();
            }
            if (SpriteCursorRaycastHit.collider.tag == "Exit Object")
            {
                print("Looking at the sphere");
                Debug.DrawRay(transform.position, transform.forward * 1000f, Color.magenta);
            }
                if (Cardboard.SDK.Triggered && SpriteCursorRaycastHit.collider.tag == "Exit Object")
            {
                SpriteCursorRaycastHit.collider.GetComponent<ObjectExit>().RecallObject();
            }
        }
    }
}
